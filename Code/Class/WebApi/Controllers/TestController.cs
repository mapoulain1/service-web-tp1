﻿using DAL.Models;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TestController : ControllerBase
    {

        private readonly SampleContext _sampleContext;

        public TestController(SampleContext sampleContext)
        {
            _sampleContext = sampleContext;
        }

        [HttpGet(Name = "GetTest")]
        public IEnumerable<Test> Get() => _sampleContext.Tests.ToArray();


        [HttpPost(Name = "PostTest")]
        public IActionResult Post(Test test)
        {
            _sampleContext.Tests.Add(test);
            _sampleContext.SaveChanges();
            return Ok();
        }


    }
}
