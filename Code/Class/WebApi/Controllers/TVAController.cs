﻿using Business;
using DTO.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TVAController : ControllerBase
    {

        private readonly DTOContext _dtoContext;

        public TVAController(DTOContext dtoContext)
        {
            _dtoContext = dtoContext;
        }


        [HttpGet(Name = "GetTVA")]
        public ProcessedProduit Get(int id)
        {
            var products = _dtoContext.Produits;
            var product = products.First(x => x.Id == id);
            if (product == null)
                return null;
            return TVAProcessor.Process(product);
        }
    }
}
