using DTO.Models;
using Moq;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest
{
    public class Test
    {

        private readonly ITestOutputHelper _output;

        public Test(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void TestTVA()
        {
            var product = Mock.Of<Produit>();
            
            _output.WriteLine($"{product.Id} {product.Nom} {product.Prix}");

        }
    }
}