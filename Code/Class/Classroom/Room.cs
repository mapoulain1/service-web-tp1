﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classroom
{
    public class Room
    {
        public string Name { get; private set; }

        public Room(string name)
        {
            Name = name;
        }
        public override string ToString()
        {
            return Name;
        }
    }
}
