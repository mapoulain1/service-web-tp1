﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classroom
{
    public class Lecture
    {
        public string Name { get; private set; }
        public IEnumerable<string> Teachers { get; private set; }
        public IEnumerable<Room> Rooms { get; private set; }
        public IEnumerable<Student> Students { get; private set; }

        public Lecture(string name)
        {
            Name = name;
            Teachers = new List<string>();
            Rooms = new List<Room>();
            Students = new List<Student>();
        }

        public void AddRoom(Room room) => (Rooms as List<Room>).Add(room);
        public void AddStudent(Student student) => (Students as List<Student>).Add(student);
        public void AddTeacher(string teacher) => (Teachers as List<string>).Add(teacher);
        public Student Find(string name) => (Students as List<Student>).Find(x => x.Name == name);

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append($"Lecture : {Name} (");
            if ((Rooms as List<Room>).Count == 0) sb.Append("no room");
            foreach (var room in Rooms)
            {
                sb.Append($"{room} ");
            }
            sb.Remove(sb.Length - 1, 1);
            sb.Append(")");
            return sb.ToString();
        }
    }
}
