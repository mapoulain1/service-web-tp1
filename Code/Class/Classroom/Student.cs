﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classroom
{
    public class Student
    {
        public string Name { get; private set; }
        public string Surname { get; private set; }
        public Promo Promo { get; private set; }
        public DateTime Birth { get; private set; }


        public Student(string name, string surname, Promo promo, int birthYear)
        {
            Name = name;
            Surname = surname;
            Promo = promo;
            Birth = new DateTime(birthYear, 1, 1);
        }

        public override string ToString()
        {
            return $"{Name} {Surname} {Birth.Year} {Promo}";
        }



    }
}
