﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classroom
{
    public class Promo
    {
        public int Year { get; private set; }

        public Promo(int year)
        {
            Year = year;
        }

        public override string ToString()
        {
            return $"Promo_{Year}";
        }
    }
}
