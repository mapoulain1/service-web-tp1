﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Classroom
{
    public class Program
    {
        public static void Main(string[] args)
        {
            HandleApplication();
        }


        private static void HandleApplication()
        {
            List<Lecture> lectures = new List<Lecture>();
            Lecture current = null;



            var input = Console.ReadLine();

            while (input != "exit")
            {
                var splitted = input.Split(' ');
                var command = splitted[0];

                switch (command)
                {
                    case "print":
                        if (current == null)
                        {
                            Console.WriteLine("No lecture selected.");
                            break;
                        }
                        if (splitted.Length == 3)
                        {
                            var name = splitted[1];
                            var found = current.Find(name);
                            Console.WriteLine(found == null ? "Student not found..." : found.ToString());
                        }
                        else
                        {
                            Console.WriteLine("Wrong arguments:  print [name]");
                        }
                        break;
                    case "ls":
                        Console.WriteLine($"Number : {lectures.Count}");
                        foreach (var lecture in lectures)
                        {
                            Console.WriteLine(lecture);
                            foreach (var student in lecture.Students)
                            {
                                Console.WriteLine($"\t{student}");
                            }
                        }

                        break;
                    case "add":
                        if (current == null)
                        {
                            Console.WriteLine("No lecture selected.");
                            break;
                        }

                        if (splitted.Length >= 4)
                        {
                            var name = splitted[1];
                            var surname = splitted[2];
                            var promo = int.Parse(splitted[3]);
                            var birthYear = int.Parse(splitted[4]);
                            var student = new Student(name, surname, new Promo(promo), birthYear);
                            current.AddStudent(student);
                            Console.WriteLine($"Student added : {student}");
                        }
                        else
                        {
                            Console.WriteLine("Wrong arguments:  add [name] [surname] [promo] [birth year]");
                        }
                        break;

                    case "addlecture":
                        if (splitted.Length >= 2)
                        {
                            current = new Lecture(splitted[1]);
                            lectures.Add(current);
                            Console.WriteLine($"Lecture added : {splitted[1]}");
                        }
                        else
                        {
                            Console.WriteLine("Wrong arguments:  addlecture [name]");
                        }
                        break;

                    case "addroom":
                        if (current == null)
                        {
                            Console.WriteLine("No lecture selected.");
                            break;
                        }
                        if (splitted.Length >= 2)
                        {
                            current.AddRoom(new Room(splitted[1]));
                            Console.WriteLine($"Room added : {splitted[1]}");
                        }
                        else
                        {
                            Console.WriteLine("Wrong arguments:  addroom [name]");
                        }
                        break;
                    default:
                        Console.WriteLine($"Unknown command \"{command}\"");
                        break;

                }
                Console.WriteLine();
                input = Console.ReadLine();
            }
        }
    }
}
