﻿using System;
using System.Collections.Generic;

namespace DataAccess.Models
{
    public partial class Employee
    {
        public int EmployeeId { get; set; }
        public string? Name { get; set; }
    }
}
