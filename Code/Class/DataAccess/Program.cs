﻿using DataAccess.Models;
using System.Linq;

namespace DataAccess
{
    public class Program
    {
        public static void Main(string[] args)
        {
            using (var db = new SampleContext())
            {

                Console.WriteLine("product with id 716");
                Console.WriteLine(db.Products.First(x => x.ProductId == 716));

                Console.WriteLine("product with black color");
                Console.WriteLine(db.Products.FirstOrDefault(x => x.Color == "Black"));

                var emp = new Employee
                {
                    Name = "Michel Forever",
                };
                db.Employees.Add(emp);
                db.SaveChanges();

                Console.WriteLine("product with id 6969");
                Console.WriteLine(db.Employees.FirstOrDefault(x => x.Name== "Michel Forever"));



            }

        }
    }
}