﻿using System;
using System.Collections.Generic;

namespace DTO.Models
{
    public partial class Produit
    {
        public int Id { get; set; }
        public string? Nom { get; set; }
        public float? Prix { get; set; }


        public Produit() : this(0, null, null)
        {

        }

        public Produit(int id, string? nom, float? prix)
        {
            Id = id;
            Nom = nom;
            Prix = prix;
        }
    }
}
