﻿using DTO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class ProcessedProduit : Produit
    {
        public double? TauxTVA { get; set; }
        public double? PrixTTC { get; set; }

        public ProcessedProduit(Produit prod) : base(prod.Id, prod.Nom, prod.Prix)
        {
            TauxTVA = null;
            PrixTTC = null;
        }
    
    
    }
}
