﻿using DTO.Models;

namespace Business
{
    public static class TVAProcessor
    {
        public const double TVARate = 0.20;


        public static ProcessedProduit Process(Produit produit)
        {
            return new ProcessedProduit(produit)
            {
                TauxTVA = TVARate,
                PrixTTC = produit.Prix * (1 + TVARate)
            };
            
        }

    }
}